<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package blm_basic
 */

get_header(); ?>

<div id="main" class="container">
	<div class="row">
	
		<section id="content" class="col-8 push-2">
			
			<header id="page-header" class="page-header col-12">
				<div class="flexcontainer">
					<div class="item first">
						<h1 class="page-heading">Search results for</h1>
					</div>
					<div class="item last">
						<h2 class="sub-title"><?php printf( __( ' %s', 'blm_basic' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
					</div>
				</div>
			</header>
			
		
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
					<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
			
					<?php the_excerpt(); ?>
				
					<?php get_template_part( 'inc/meta' ); ?>

				</article>
		
			 <?php endwhile; else: ?>
				 
	 			<article class="no-results">
		
					<p>Sorry, no posts matched your criteria. Please try another keyword.</p>
				   
				</article>
	
	 	  	 <?php endif; ?>
	
			<?php blm_basic_paging_nav(); ?>
	
		</section><!-- #content -->

	</div><!-- .row -->
</div><!-- #main -->

<?php get_footer(); ?>
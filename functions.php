<?php
/**
 * BLM Basic Starter Theme functions and definitions
 *
 * @package blm_basic
 */


/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'blm_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function blm_theme_setup() {


	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Add theme support for post thumbnails (featured images). 
	add_theme_support( 'post-thumbnails' );
	add_image_size ( 'large-headshot', 250, 250, true);
	add_image_size ( 'portfolio-thumb', 180, 100, true);
	
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );


	// Add theme support for menus
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'blm_basic' ),
	) );
	
	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

}
endif; // blm_theme_setup
add_action( 'after_setup_theme', 'blm_theme_setup' );


/**
 * Register widget area
 */

function blm_widgets_init() {
	register_sidebar( array(
		'id' => 'primary',
		'name' => __( 'Primary Sidebar', 'blm_basic' ),
		'description' => __( 'The widget is used to display popular posts on your blog section.', 'blm_basic' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
	) );
		
	register_sidebar( array(
		'id' => 'secondary',
		'name' => __( 'Subscription bar', 'blm_basic' ),
		'description' => __( 'This widget is used to display the form on your home page and blog.', 'blm_basic' ),
		'before_widget' => '<aside id="%1$s" class="widget-single %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
	) );
		
		register_sidebar( array(
			'id' => 'subscribe',
			'name' => __( 'Subscribe From', 'blm_basic' ),
			'description' => __( 'The widget is used to display a subscription on your subscribe page.', 'blm_basic' ),
			'before_widget' => '<aside id="%1$s" class="widget-page %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h4>',
			'after_title' => '</h4>'
		) );
}
add_action( 'widgets_init', 'blm_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function blm_basic_scripts() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	
	wp_enqueue_style('googleFonts', 'http://fonts.googleapis.com/css?family=Roboto:400,500,400italic,500italic,700,700italic' );	
	wp_enqueue_style( 'font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css', array(), '', 'all' );

	wp_enqueue_script('jquery');
	wp_enqueue_script( 'blm_navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	
	wp_enqueue_script( 'blm_custom', get_template_directory_uri() . '/js/custom.js', array( 'jquery' ), '20120206', true );
	//wp_enqueue_script( 'blm_custom', get_template_directory_uri() . '/js/custom.js' );
	
	wp_enqueue_script( 'blm-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	wp_enqueue_script( 'lazyload',  get_template_directory_uri().'/js/jquery.lazyload.min.js',  array('jquery'), '', true );
	wp_enqueue_script( 'isotope',  get_template_directory_uri().'/js/isotope.min.js',  array('jquery'), '', true );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'blm_basic_scripts' );



//Remove eLlipses from excerpt

function blm_custom_excerpt_more($more) {
	return '...<br /><a href="'. get_permalink() .'" class="more-link">Read more <i class="fa fa-angle-right"></i></a>';
	}
add_filter('excerpt_more', 'blm_custom_excerpt_more');


//Modify the excerpt length

function blm_custom_excerpt_length($length) {
	if (is_front_page() ){
		return 18;
	}
	else {
		return 50;
	}
	
}
add_filter('excerpt_length', 'blm_custom_excerpt_length');

// Custom post navigation for theme.
require get_template_directory() . '/inc/page-navi.php';

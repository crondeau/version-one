jQuery(window).load(function(){	
	// initialize Isotope
	var jQuerycontainer = jQuery('.isotope-list'),
	filters = {};
	jQuerycontainer.isotope({
		itemSelector : '.element',
		layoutMode: 'masonry',
		isResizeBound: true,
		filter: '.current-portfolio' // remove the display all and showing current-portfolio first.
		
	});

	jQuery('.filter-wrapper').on('click','a',function(e){
		e.preventDefault();
		var filterTerm = jQuery(this).attr('data-filter-name');
		jQuery(this).addClass('active').siblings().removeClass('active');
		
		jQuerycontainer.isotope({
			filter: filterTerm
		});
		jQuery(filterTerm).each(function(){
			var img = jQuery(this).find('img');
			var src = img.attr('data-src');
			img.attr('src',src)
		});
	});

	jQuerycontainer.removeClass("loading"); 
	jQuery('.filter-container, .filters').animate({
		opacity: 1
	}, 1000);
	
});

jQuery(document).ready(function($) {
	function navMenu() {

		var searchToggle = $('#search-toggle');

		var searchNav = $('#search-toggle-nav');

		function myToggleClass( $myvar ) {
			if ( $myvar.hasClass( 'active' ) ) {
				$myvar.removeClass( 'active' );
			} else {
				$myvar.addClass('active');
			}
		}

		// Display/hide search
		searchToggle.on('click', function() {
			searchNav.slideToggle();
			myToggleClass($(this));
		});
	}
	$(window).on('load', navMenu);
} );
<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section, header and top navigation areas
 *
 * @package blm_basic
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title(); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/images/icons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/images/icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/images/icons/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/images/icons/manifest.json">
<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/images/icons/safari-pinned-tab.svg" color="#eb1d29">
<meta name="theme-color" content="#ffffff">

<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>

<script type="text/javascript" src="//use.typekit.net/kju8ify.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script src="https://static.ada.support/embed.29518ede.min.js" charset="utf-8"></script>

<!-- Facebook Pixel Code -->
<?php if(is_page(2563)) : ?>
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '1196934117078129'); 
	fbq('track', 'PageView');
	fbq('track', 'CompleteRegistration');
</script>

<?php else : ?>
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '1196934117078129'); 
	fbq('track', 'PageView');
</script>
<?php endif; ?>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=1196934117078129&ev=PageView
&noscript=1"/>
</noscript>	
<!-- End Facebook Pixel Code -->
</head>
<body <?php body_class(); ?>>
<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'blm_basic' ); ?></a>
	<nav id="site-navigation" class="main-navigation hide-on-large" role="navigation">
		<button class="menu-toggle"><?php _e( 'Menu', 'blm_basic' ); ?></button>
		<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
	</nav>
	<div id="search-toggle-nav" class="panel">
		<div class="row">
			<div class="search-wrapper push-8 col-4 last">
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>

<header id="masthead" class="site-header container" role="banner">
	<div class="row">
		<div id="branding" class="col-3">
			<h1 id="logo"><a href="<?php echo home_url() ?>/"><?php bloginfo( 'name' ); ?></a></h1>
		</div>
		
		<nav id="site-navigation" class="main-navigation col-9 last hide-on-small" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		</nav>
		<div class="toggles hide-on-small">
			<div id="search-toggle" class="toggle" title="<?php esc_attr_e( 'Search', 'blm_basic' ); ?>"><i class="fa fa-search"></i><span class="screen-reader-text"><?php _e( 'Search', 'blm_basic' ); ?></span></div>
		</div>
	</div>		
</header>		
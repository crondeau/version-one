<?php
/**
 * This template will be used to display page the team page in two columns.
 *
 * @package blm_basic
 */

get_header(); ?>

<div id="main" class="container">
	<div class="row">
		
	<section id="content" class="col-10 push-1">
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<header id="page-header" class="page-header col-12">
			<div class="flexcontainer">
				<div class="item first">
					<h1 class="page-heading"><?php the_title(); ?></h1>
				</div>
				<div class="item last">
					<h2 class="sub-title"><?php the_field( 'headline' ); ?></h2>
				</div>
			</div>
		</header>

		<div class="col-6">
			<div class="bio">
				<h2>Boris Wertz</h2>
			
				<?php if( get_field( 'headshot_boris' ) ): ?>

					<?php $image = wp_get_attachment_image_src( get_field( 'headshot_boris' ), 'large-headshot' ); ?> 
 
					<img class="aligncenter" src="<?php echo esc_url( $image[0] ); ?>" alt="Boris Wertz"> 

				<?php endif; ?>
			
				<ul class="contact-details">
					<li><a href="http://twitter.com/<?php the_field( 'twitter_boris' )?>" title="follow Boris on Twitter" target="_blank"><i class="fa fa-twitter fa-2x"></i></a></li>
					<li><a href="<?php the_field( 'linkedin_boris' )?>" title="Connect to Boris on LinkedIn" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a></li>
					<li><a href="mailto:<?php the_field( 'boris_email_address' )?>" title="Contact Boris"><i class="fa fa-envelope fa-2x"></i></a></li>
				</ul>
				<p class="align-center small"><?php the_field( 'location_boris' ); ?></p>
			
				<?php the_field( 'bio_boris' ); ?>
			</div>
		</div>
		
		<div class="col-6 last">
			<div class="bio">
				<h2>Angela Tran Kingyens</h2> 
			
				<?php if( get_field( 'headshot_angela' ) ): ?>

					<?php $image = wp_get_attachment_image_src( get_field( 'headshot_angela' ), 'large-headshot' ); ?> 
 
					<img class="aligncenter" src="<?php echo esc_url( $image[0] ); ?>" alt="Angela Tran Kingyens"> 

				<?php endif; ?>
			
				<ul class="contact-details">
					<li><a href="http://twitter.com/<?php the_field( 'twitter_angela' )?>" title="follow Angela on Twitter" target="_blank"><i class="fa fa-twitter fa-2x"></i></a></li>
					<li><a href="<?php the_field( 'linkedin_angela' )?>" title="Connect to Angela on LinkedIn" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a></li>
					<li><a href="mailto:<?php the_field( 'angela_email_address' )?>" title="Contact Angela"><i class="fa fa-envelope fa-2x"></i></a></li>
				</ul>
				<p class="align-center small"><?php the_field( 'location_angela' ); ?></p>
			
				<?php the_field( 'bio_angela' ); ?>
			</div>
		</div>
	
		
		<?php endwhile; endif; ?>
		
	</section><!-- #content -->

	</div>
</div><!-- #main -->

<?php get_footer(); ?>
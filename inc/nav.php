<?php
/**
 * This template generates the links to previous and next posts on the single template
 *
 * @package blm_basic
 */
?>

<nav class="post-navigation">
	<div class="nav-previous"><?php next_posts_link( __( '<i class="fa fa-chevron-circle-left"></i> Previous', 0 ) ); ?></div>
	<div class="nav-next"><?php previous_posts_link( __( 'Next <i class="fa fa-chevron-circle-right"></i>', 0 ) ); ?></div>
</nav>
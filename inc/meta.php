<?php
/**
 * This template generates the meta information below each post. 
 * Link to Author, Date, Number of comments
 * List which category and tags are associated with this post
 *
 * @package blm_basic
 */
?>

	<div class="entrymeta row">
		<div class="author-pic hide-on-small">
			<?php the_author_image($author_id = null); ?>
		</div>
	
		<div class="meta-info">
			<p><?php the_time( 'F d, Y' ); ?> <span class="sep"> | </span> <?php comments_popup_link( 'No Comments <i class="fa fa-angle-right"></i>', '1 Comment <i class="fa fa-angle-right"></i>', '% Comments <i class="fa fa-angle-right"></i>' ); ?></p>
	
			<?php echo get_the_tag_list('<p class="tag-list"><i class="fa fa-tag"></i> ',' ','</p>'); ?>
		</div>
	</div>

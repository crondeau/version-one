<?php
/**
 * This template will be used to display page content.
 *
 * @package blm_basic
 */

get_header(); ?>

<div id="intro" class="container">
	<div class="row">
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
		<div class="banner-header">
			<h1 class="front-page-heading align-center"><?php the_field( 'headline' ); ?></h1>
		</div>

		<div class="hidden">
			<?php the_content(); ?>
		</div>
		
	</div>
</div>
	
<div id="main" class="container">
	
	<div class="row">
	
		<section id="port-news" class="col-12">
			<h2>Our Portfolio Companies in the News</h2>
			<ul class="latest-posts">
				<?php while( the_repeater_field( 'news_piece' ) ): ?>
					
					<?php $image = wp_get_attachment_image_src( get_sub_field( 'news_image' ), 'medium' ); ?> 
	
				<li>
					<div class="pic" style="background: url(<?php echo esc_url( $image[0] ); ?>) no-repeat center center; 
					  -webkit-background-size: cover;
					  -moz-background-size: cover;
					  -o-background-size: cover;
					  background-size: cover;"></div>
					<div class="content-entry narrow">
						<h3 class="post-title"><a href="<?php the_sub_field( 'news_link' ); ?>" target="_blank"><?php the_sub_field( 'news_title' ); ?></a></h3>			
					</div>
				</li>		

				<?php endwhile;  ?>
			
			</ul>
			<p class="align-center"><a href="<?php echo home_url() ?>/our-portfolio/" class="button">View full portfolio</a></p>
			

		</section><!-- #port-news -->

	</div>
	
	<?php endwhile; endif; ?>
	
	
	<div class="row">
		<section id="blog-news" class="col-12">
			
			<h2>Latest Posts</h2>
			<ul class="latest-posts">
			<?php 
				$new_query_1 = new WP_Query();
				$new_query_1->query(array('posts_per_page' => 3, ));
				?>
				<?php while ($new_query_1->have_posts()) : $new_query_1->the_post(); ?>
				<li>
					<div class="content-entry">
					<h3><?php the_title(); ?></h3>
						<?php the_excerpt(); ?>
					</div>
				</li>
			<?php endwhile; ?>
			</ul>
			
			<p class="align-center"><a href="<?php echo home_url() ?>/blog/" class="button">Version One blog</a></p>

		</section><!-- #blog-news -->

	</div>
</div><!-- #main -->
<div class="container">
	<div class="row">
		<div id="subscribe-form" class="subscribe col-12">
			<?php if ( ! dynamic_sidebar( 'secondary' ) ) : ?>

			<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
<?php
/**
 * Template Name: Popular tags.
 *
 * @package blm_basic
 */

get_header(); ?>

<div id="main" class="container">
	<div class="row">
		
	<section id="content" class="col-8 push-2">
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<header id="page-header" class="page-header col-12">
			<div class="flexcontainer">
				<div class="item first">
					<h1 class="page-heading"><?php the_title(); ?></h1>
				</div>
				<div class="item last">
					<h2 class="sub-title"><?php the_field( 'headline' ); ?></h2>
				</div>
			</div>
		</header>
	
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<?php the_content(); ?>
			
			<?php if ( ! dynamic_sidebar( 'subscribe' ) ) : ?>


			<?php endif; ?>
	
		</article>
		
		

		<?php endwhile; endif; ?>

	</section><!-- #content -->

	</div>
</div><!-- #main -->

<?php get_footer(); ?>
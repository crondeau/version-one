<?php
/**
 * Template Name: Portfolio
 *
 * @package blm_basic
 */

get_header(); ?>

<div id="main" class="container">
	<div class="row">

		<header id="page-header" class="page-header col-12">
			<div class="flexcontainer">
				<div class="item first">
					<h1 class="page-heading"><?php echo get_the_title( 2197 ); ?></h1>
				</div>
				<div class="item last">
					<h2 class="sub-title"><?php the_field( 'headline', 2197 ); ?></h2>
				</div>
			</div>
		</header>

		
	<section id="content">
		
		<div id="portfolio-filter">
			<div class="filters sorting-block" style="clear:both;">

				<div class="filter-wrapper portfolio-types">
					
					
					<?php 
						$terms = get_terms('portfolio-type');						
						foreach($terms as $term) {
							echo "<a  data-filter-name='.".sanitize_title($term->slug)."'><span>" . $term->name . "</span></a>";
						}
					?>

				</div>

				<ul class="portfolio-list isotope-list">

					<?php
					$args = array(
						'post_type' => 'portfolio',
						'posts_per_page' => -1,
						'orderby' => 'menu_order',
						'order' => 'ASC',
					); 
					$grid = new WP_Query($args);
					if($grid->have_posts()): 
						while($grid->have_posts()): $grid->the_post();

						$classes = ''; 

						$project_terms = get_the_terms($post->ID, 'portfolio-type'); 
						if ( !empty( $project_terms ) && !is_wp_error( $project_terms ) ){
							foreach($project_terms as $term ){
								$classes .= ' '.$term->slug;
							}
						}
					?>
						<li class="element <?php echo $classes; ?>">

							<div class="portfolio-image">

								<a href="<?php the_field( 'portfolio_url' ); ?>" target="_blank">
										<?php 
										$thumbnail_id = get_post_thumbnail_id(  ); 
										$thumbnail = wp_get_attachment_image_src( $thumbnail_id, 'portfolio-thumb' ); ?>
										<div class="image-wrap">
											<img class="lazy" src="<?php echo get_template_directory_uri(); ?>/images/placeholder.png" alt="" data-src="<?php echo $thumbnail[0] ?>">
										</div>
									<div class="portfolio-overlay" style="display: none;">

										<div class="portfolio-overlay-wrap">
											<h4><?php the_title(); ?></h4>
											<?php the_content(); ?>
										</div>

									</div>
								</a>

							</div>

						</li>

					<?php endwhile; endif; wp_reset_query(); ?>

				</ul>
			</div>
		</div>
		
	</section><!-- #content -->

	</div>
</div><!-- #main -->

<?php get_footer(); ?>
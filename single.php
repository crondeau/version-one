<?php
/**
 * The Template for displaying all single posts.
 *
 * @package blm_basic
 */

get_header(); ?>

<div id="main" class="container">
	<div class="row">
	
		<section id="content" class="col-8 push-2">
		
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
					<h1 class="post-heading"><?php the_title(); ?></h1>
					
						<p class="date">By <?php the_author();?>, <?php the_time( 'F d, Y' ); ?></p>
			
					<?php the_content(); ?>

				</article>
				

				<?php if ( ! dynamic_sidebar( 'secondary' ) ) : ?>

				<?php endif; ?>

				<?php comments_template(); ?>
	
			<?php endwhile; endif; ?>
	
		</section>

	</div><!-- .row -->
</div><!-- #main -->

<?php get_footer(); ?>
<?php
/**
 * Master Template. This template will be used to display your blog posts and pages if page.php does not exist.
 *
 * @package blm_basic
 */

get_header(); ?>

<div id="main" class="container">
	<div class="row">

		<section id="" class="col-8 push-2">
			
			<header id="page-header" class="page-header col-12">
				<div class="flexcontainer">
					<div class="item first">
						<h1 class="page-heading"><?php echo get_the_title( 9 ); ?></h1>
					</div>
					<div class="item last">
						<h2 class="sub-title"><?php the_field( 'headline', 9 ); ?></h2>
					</div>
				</div>
			</header>
			
		</section>
			
		<?php if(!is_paged() ): ?>
			
		<section id="port-news" class="col-8 push-2">

			<h2>Our favorite posts</h2>
			
			<ul class="favorite-posts">
			<?php 
				$new_query_1 = new WP_Query();
				$new_query_1->query(array('posts_per_page' => 3, 'category_name' => 'favorites' ));
				?>
				<?php while ($new_query_1->have_posts()) : $new_query_1->the_post(); 
					
					$thumb_id = get_post_thumbnail_id();
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', true);
					$thumb_url = $thumb_url_array[0];?>
				<li>
					<div class="pic" style="background: url(<?php echo $thumb_url; ?>) no-repeat center center; 
					  -webkit-background-size: cover;
					  -moz-background-size: cover;
					  -o-background-size: cover;
					  background-size: cover;"></div>
					<div class="content-entry narrow">
						<h3 class="favorite-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						
					</div>
				</li>
			<?php endwhile; ?>
			</ul>

		</section>
		
	<?php endif; ?>
	
		<section id="content" class="col-8 push-2">	
			
		<?php if(!is_paged() ): ?>
			<h2>Latest posts</h2>
		<?php endif; ?>	
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
			
			<?php the_excerpt(); ?>
				
			<?php get_template_part( 'inc/meta' ); ?>

		</article>
		
		<?php endwhile; endif; ?>
	
		<?php blm_basic_paging_nav(); ?>
	
		</section><!-- #content -->

	</div><!-- .row -->
</div><!-- #main -->

<div class="container">
	<div class="row">
		<div id="subscribe-form" class="subscribe col-8 push-2">	
			<?php if ( ! dynamic_sidebar( 'secondary' ) ) : ?>

			<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
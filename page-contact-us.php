<?php
/**
 * This template will be used to display page the team page in two columns.
 *
 * @package blm_basic
 */

get_header(); ?>

<div id="main" class="container">
	<div class="row">
		
	<section id="content" class="col-8 push-2">
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
			<header id="page-header" class="col-12">
				<div class="main-head col-3">
					<h1 class="page-heading"><?php the_title(); ?></h1>
				</div>
				<div class="sub-head col-7">
					<h2 class="sub-title"><?php the_field( 'headline' ); ?></h2>
				</div>
			</header>
			
			<?php the_content(); ?>

				<div class="row contact-details">
			
					<div class="col-6">
						<h2>Vancouver</h2>
						<img src="<?php the_field( 'vancouver_map' ); ?>" alt="Vancouver" class="location-map">	
						<?php the_field( 'vancouver_contact' ); ?>
					</div>
			
					<div class="col-6 last">
						<h2>San Francisco</h2>	
						<img src="<?php the_field( 'palo_map' ); ?>" alt="Palo Alto" class="location-map">
						<?php the_field( 'palo_alto_contact' ); ?>
					</div>
				</div>
		
		<?php endwhile; endif; ?>
		
	</section><!-- #content -->

	</div>
</div><!-- #main -->

<?php get_footer(); ?>
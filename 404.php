<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package blm_basic
 */

get_header(); ?>

<div id="main" class="container">
	<div class="row">
		
	<section id="content" class="col-8 push-2">

		<article class="hentry">		
			<h1>Page not found.</h1>
			<p>We've recently made changes to our website and the page you are looking for might have been deleted or moved. Please <a href="<?php echo home_url(); ?>">visit our home page instead</a>.</p>
			<p style="padding-bottom: 200px;">Sorry for the inconvenience.</p>		
		</article>

	</section><!-- #content -->

	</div>
</div><!-- #main -->

<?php get_footer(); ?>
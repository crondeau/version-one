<?php
/**
 * The template for displaying the footer.
 * 
 * @package blm_basic
 */
?>

<footer id="footer" class="container">
	<div class="row">
		<ul>
			<li>&copy; <?php echo date( 'Y' ); ?> <?php bloginfo( 'name' ); ?></li>
			<li class="sep"></li>
			<li><a href="<?php echo home_url() ?>/feed/" target="_blank">rss</a></li>
			<li><a href="https://twitter.com/VersionOneVC" target="_blank">versiononevc</a></li>
			<li class="sep"></li>
			<li><a href="<?php echo home_url() ?>/about-us/">people</a></li>
			<li><a href="<?php echo home_url() ?>/philosophy/">core beliefs</a></li>
			<li><a href="<?php echo home_url() ?>/our-portfolio/">portfolio</a></li>
			<li><a href="<?php echo home_url() ?>/blog/">blog</a></li>
			<li><a href="<?php echo home_url() ?>/populartopics/">popular topics</a></li>
		</ul>		
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>